package com.saqq.sweetsupgrade.menu;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.saqq.sweetsupgrade.GameActivity;
import com.saqq.sweetsupgrade.R;
import com.saqq.sweetsupgrade.objects.SaveState;

public class MenuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);

		Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/sweet.ttf");
		TextView titleView = (TextView) findViewById(R.id.title);
		titleView.setTypeface(tf);
		TextView startView = (TextView) findViewById(R.id.start);
		startView.setTypeface(tf);
		TextView highscoresView = (TextView) findViewById(R.id.highscores);
		highscoresView.setTypeface(tf);
		TextView optionsView = (TextView) findViewById(R.id.options);
		optionsView.setTypeface(tf);
		TextView aboutView = (TextView) findViewById(R.id.about);
		aboutView.setTypeface(tf);
		TextView exitView = (TextView) findViewById(R.id.exit);
		exitView.setTypeface(tf);
		TextView resume = (TextView) findViewById(R.id.resume);
		resume.setTypeface(tf);
		if((new SaveState(this).ifSaved()))
			resume.setEnabled(true);
		else 
			resume.setEnabled(false);
	}
	

	public void exitApp(View view) {
		finish();
	}

	public void optionsStart(View view) {
		Intent intent = new Intent(this, OptionsActivity.class);
		startActivity(intent);
	}

	public void highscoresStart(View view) {
		Intent intent = new Intent(this, HighscoresActivity.class);
		startActivity(intent);
	}

	public void gameStart(View view) {
		SaveState ss = new SaveState(this);
		ss.deleteSave();
		Intent intent = new Intent(this, GameActivity.class);
		startActivity(intent);
	}

	public void resume(View view) {
		Intent intent = new Intent(this, GameActivity.class);
		startActivity(intent);
	}

	public void aboutStart(View view) {
		Intent intent = new Intent(this, AboutActivity.class);
		startActivity(intent);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		TextView resume = (TextView) findViewById(R.id.resume);
		if((new SaveState(this).ifSaved())) {
			resume.setEnabled(true);
		} else {
			resume.setEnabled(false);
		}
	}

}
