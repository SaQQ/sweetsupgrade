package com.saqq.sweetsupgrade.menu;

import com.saqq.sweetsupgrade.R;
import com.saqq.sweetsupgrade.objects.Highscore;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class HighscoresActivity extends Activity {
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_highscores);
		highscores = new Highscore(this);
		String [] highscoreTable = new String[10];
		String highscoresString = "";
		highscoreTable = highscores.getScore();
		for(int i = 0; i < Highscore.DEFAULT_HIGHSCORE_SIZE; i++){
			highscoresString += "#" + (i+1) +" "+ highscoreTable[i] + "\n";
		}
		Typeface tf = Typeface.createFromAsset(getAssets(),"fonts/sweet.ttf");
		TextView highscoresView = (TextView) findViewById(R.id.highscores);
		highscoresView.setTypeface(tf);
		highscoresView.setText(highscoresString);
	}
	Highscore highscores;

}
