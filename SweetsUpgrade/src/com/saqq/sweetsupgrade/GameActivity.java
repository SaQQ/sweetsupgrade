package com.saqq.sweetsupgrade;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.DialogFragment;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

import com.saqq.sweetsupgrade.menu.FinishDialogFragment;
import com.saqq.sweetsupgrade.renderer.GLRenderer;

public class GameActivity extends Activity {

	private GameView gv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		gv = new GameView(this);
		
		final ActivityManager activityManager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
		final boolean supportsES2 = configurationInfo.reqGlEsVersion >= 0x20000;
		
		if(supportsES2) {
			GLRenderer renderer = GLRenderer.getInstance();
			renderer.setContext(this);
			renderer.setGameView(gv);
			gv.setEGLContextClientVersion(2); //has to be called before setRenderer()
			gv.setRenderer(renderer);

		} else {
			Log.e("GLES20 NOT SUPPORTED", "EXITING");
			this.finish();
			return;
		}
		
		setContentView(gv);

	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return gv.onTouchEvent(event);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		gv.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		gv.onPause();
	}
	
	protected void onStop() {
		super.onStop();
	}
	
	@SuppressLint("NewApi")
	public void showFinishDialog(int score) {
	    DialogFragment newFragment = FinishDialogFragment.newInstance(
	            score);
	    newFragment.show(getFragmentManager(), "dialog");
	}
	
	public GameView getView() {
		return this.gv;
	}
	
}
