package com.saqq.sweetsupgrade.renderer;

import static android.opengl.GLES20.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

public class ShaderUtils {

	public static String getShaderSourceCode(Context context, int resID) {
		Resources r = context.getResources();
		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(r.openRawResource(resID)));
		
		String s;
		
		try {
			while((s = br.readLine()) != null)
				sb.append(s);
			br.close();
		} catch (IOException e) { }
		
		return sb.toString();
	}
	
	public static int compileShader(final int shaderType, final String shaderSource) {
		
		int shaderHandle = glCreateShader(shaderType);
		
		if(shaderHandle != 0) {
			glShaderSource(shaderHandle, shaderSource);
			glCompileShader(shaderHandle);
			final int[] compileStatus = new int[1];
			glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, compileStatus, 0);
			if(compileStatus[0] == 0) {
				Log.e("ShaderUtils", "Error compiling shader: " + glGetShaderInfoLog(shaderHandle));
				glDeleteShader(shaderHandle);
				shaderHandle = 0;
			}
		}
		
		if(shaderHandle == 0)
			throw new RuntimeException("Error creating shader");
		else
			Log.d("ShaderUtils", "Succesfully compiled shader: " + glGetShaderInfoLog(shaderHandle));
		
		return shaderHandle;
		
	}
	
	public static int createAndLinkProgram(final int vertexShaderHandle, final int fragmentShaderHandle, final String[] attributes) {
		
		int programHandle = glCreateProgram();
		
		if(programHandle != 0) {
			glAttachShader(programHandle, vertexShaderHandle);
			glAttachShader(programHandle, fragmentShaderHandle);
			if(attributes != null) {
				final int size = attributes.length;
				for(int i = 0; i < size; i++) {
					glBindAttribLocation(programHandle, i, attributes[i]);
				}
			}
			glLinkProgram(programHandle);
			final int[] linkStatus = new int[1];
			glGetProgramiv(programHandle, GL_LINK_STATUS, linkStatus, 0);
			
			if(linkStatus[0] == 0) {
				Log.e("GLRenderer", "Error linking program: " + glGetProgramInfoLog(programHandle));
				glDeleteProgram(programHandle);
				programHandle = 0;
			}
		}
		
		if(programHandle == 0)
			throw new RuntimeException("Error creating program");
		else
			Log.d("ShaderUtils", "Succesfully linked program: " + glGetProgramInfoLog(programHandle));
		return programHandle;
	}
	
	public static void deleteShader(final int shaderHandle) {
		glDeleteShader(shaderHandle);
	}
	
	public static void deleteProgram(final int programHandle) {
		glDeleteProgram(programHandle);
	}
}
