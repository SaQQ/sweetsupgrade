package com.saqq.sweetsupgrade.renderer;

import static android.opengl.GLES20.GL_BLEND;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_TEST;
import static android.opengl.GLES20.GL_FRAGMENT_SHADER;
import static android.opengl.GLES20.GL_LEQUAL;
import static android.opengl.GLES20.GL_ONE_MINUS_SRC_ALPHA;
import static android.opengl.GLES20.GL_SRC_ALPHA;
import static android.opengl.GLES20.GL_VERTEX_SHADER;
import static android.opengl.GLES20.glBlendFunc;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glClearDepthf;
import static android.opengl.GLES20.glDepthFunc;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform1f;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glViewport;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.Matrix;
import android.util.Log;

import com.saqq.sweetsupgrade.GameView;
import com.saqq.sweetsupgrade.R;
import com.saqq.sweetsupgrade.objects.ProgressDisplay;

public class GLRenderer implements Renderer {
	
	private static final String TAG = "GLRenderer";
	
	private static final boolean LOG_FPS = true;
	
	private static Context context;
	
	private static GLRenderer instance;
	
	private float[] mViewMatrix = new float[16];
	private float[] mProjectionMatrix = new float[16];	
	private float[] mMVPMatrix = new float[16];
	private float[] mMVMatrix = new float[16];
	
	//MATRIX STACK
	private static final int MATRIX_STACK_SIZE = 8;
	private float[][] sModelMatrixStack = new float[MATRIX_STACK_SIZE][16];
	private int iStackPointer = 0;
	
	private int basicProgramHandle;
	private int progressProgramHandle;
	
	private int mMVPMatrixHandle;
	private int mMVMatrixHandle;
	
	private int mTexUniformHandle;
	private int mAlphaHandle;
	
	//**************FPS**************//
	private int frames = 0;
	private long lastCheck;
	private long timeBegin;
	
	//************OBJECTS************//
	private GameView gameView;
	
	private GLRenderer() {
		Matrix.setIdentityM(mViewMatrix, 0);
		loadIdentity();
		Matrix.setIdentityM(mProjectionMatrix, 0);
		Matrix.setIdentityM(mMVPMatrix, 0);
	}

	public static GLRenderer getInstance() {
		if (instance == null) {
			instance = new GLRenderer();
		}
		return instance;
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		
		//***this code should be executed by another thead but im too stiupid to implement it***//
		long timeDiff = System.currentTimeMillis() - timeBegin;
		gameView.update((int)timeDiff);
		timeBegin = System.currentTimeMillis();
		//**************************************************************************************//
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		loadIdentity();
		
		//translate(-width/2, -height/2, 0);
		
		 //no idea why it works placed here, not in setup
		glUniform1i(mTexUniformHandle, 0);
		
		gameView.draw(); //begin recursive shitload
		
		frames++;
		
		long currentTime = System.currentTimeMillis();
		if(currentTime - lastCheck > 1000L) {
			lastCheck = currentTime;
			if(LOG_FPS) Log.d("FPS: ", Integer.toString(frames));
			frames = 0;
		}
		
	}
	
	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {

		setupViewPort(width, height);

	}

	@Override
	public void onSurfaceCreated(GL10 unused, EGLConfig config) {

		setupOpenGL();
		
		TextureUtils.loadTextures(context);
		
		gameView.init();

	}

	private void setupOpenGL() {

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClearDepthf(1.0f);
		
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL); //no idea why but it works (means 1.z <= 2.z)
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //remember not to diffuse fucking alpha channel
		
		//BASIC SHADER LOADING
		
		basicProgramHandle = createProgram(R.raw.basicvs, R.raw.basicfs, new String[] {"a_Position", "a_Color", "a_Normal", "a_TexCoordinate"});
		progressProgramHandle = createProgram(R.raw.progressvs, R.raw.progressfs, new String[] {"a_Position", "a_Color", "a_Normal", "a_TexCoordinate"});
		ProgressDisplay.mProgramHandle = progressProgramHandle;
		
		glUseProgram(basicProgramHandle);

		mMVPMatrixHandle = glGetUniformLocation(basicProgramHandle, "u_MVPMatrix");
		mMVMatrixHandle = glGetUniformLocation(basicProgramHandle, "u_MVMatrix");
		mTexUniformHandle = glGetUniformLocation(basicProgramHandle, "u_Texture");
		mAlphaHandle = glGetUniformLocation(basicProgramHandle, "u_Alpha");
		
		Log.d(TAG, "OpenGL has been setup!");

	}

	private void setupViewPort(int width, int height) {
		glViewport(0, 0, width, height);
		
		final float w = 1.0f;
		final float h =  height/(float)width;
		
		final float left = 0.0f;
		final float right = (float)w;
		final float bottom = 0.0f;
		final float top = (float)h;
		final float near = -1.0f;
		final float far = 1.0f;
		
		/*final float left = -(float)w/2;
		final float right = (float)w/2;
		final float bottom = -(float)h/2;
		final float top = (float)h/2;
		final float near = 1.0f;
		final float far = 10.0f;*/
		
		//Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
		Matrix.orthoM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
		
		/*final float eyeX = (float)w/2;
		final float eyeY = (float)h/2;
		final float eyeZ = 1.1f;

		final float centerX = (float)w/2;
		final float centerY = (float)h/2;
		final float centerZ = 0.0f;

		final float upX = 0.0f;
		final float upY = 1.0f;
		final float upZ = 0.0f;

		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);*/
		
				
	}
	
	public void setContext(Context context) {
		GLRenderer.context = context;
	}
	
	public void setGameView(GameView gv) {
		this.gameView = gv;
	}
	
	public void translate(final float x, final float y, final float z) {
		Matrix.translateM(sModelMatrixStack[iStackPointer], 0, x, y, z);
	}
	
	public void rotate(final float angle, final float x, final float y, final float z ) {
		Matrix.rotateM(sModelMatrixStack[iStackPointer], 0, angle, x, y, z);
	}
	
	public void scale(final float x, final float y, final float z) {
		Matrix.scaleM(sModelMatrixStack[iStackPointer], 0, x, y, z);
	}
	
	public void loadIdentity() {
		Matrix.setIdentityM(sModelMatrixStack[iStackPointer], 0);
	}
	
	public void setAlpha(float alpha) {
		glUniform1f(mAlphaHandle, alpha);
	}
	
	public void pushMatrix() {
		iStackPointer++;
		if(iStackPointer > MATRIX_STACK_SIZE - 1) 
			throw new RuntimeException("Matrix Error: pushMatrix(): can't cant push above maximal stack size.");
		for(int i = 0; i < 16; i++) //because fucking clone does so much allocation that GC invokes every half of a second...
			sModelMatrixStack[iStackPointer][i] = sModelMatrixStack[iStackPointer - 1][i]; 
	}
	
	public void popMatrix() {
		iStackPointer--;
		if(iStackPointer < 0) 
			throw new RuntimeException("Matrix Error: popMatrix(): can't pop below 0.");
	}
	
	public void applyMatrix() {
		Matrix.multiplyMM(mMVMatrix, 0, mViewMatrix, 0, sModelMatrixStack[iStackPointer], 0);
		glUniformMatrix4fv(mMVMatrixHandle, 1, false, mMVMatrix, 0);
		Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVMatrix, 0);
		glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);
	}
	
	public float[] getModelMatrix() {
		return sModelMatrixStack[iStackPointer];
	}
	
	public float[] getMVMatrix() {
		float[] MVMatrix = new float[16];
		Matrix.multiplyMM(MVMatrix, 0, mViewMatrix, 0, sModelMatrixStack[iStackPointer], 0);
		return MVMatrix;
	}
	
	public float[] getMVPMatrix() {
		float[] MVPMatrix = new float[16];
		Matrix.multiplyMM(MVPMatrix, 0, mViewMatrix, 0, sModelMatrixStack[iStackPointer], 0);
		Matrix.multiplyMM(MVPMatrix, 0, mProjectionMatrix, 0, MVPMatrix, 0);
		return MVPMatrix;
	}
	
	public void printMatrix(int pointer) {
		float[] matrix = sModelMatrixStack[pointer];
		Log.d("MATRIX:", "stack pointer: " + pointer);
		Log.d("1: ", " " + matrix[0] + " " + matrix[1] + " " + matrix[2] + " " + matrix[3]);
		Log.d("2:", " " + matrix[4] + " " + matrix[5] + " " + matrix[6] + " " + matrix[7]);
		Log.d("3: ", " " + matrix[8] + " " + matrix[9] + " " + matrix[10] + " " + matrix[11]);
		Log.d("4: ", " " + matrix[12] + " " + matrix[13] + " " + matrix[14] + " " + matrix[15]);
	}
	
	public void printMatrix(float[] matrix) {
		Log.d("MATRIX:", " ");
		Log.d("1: ", " " + matrix[0] + " " + matrix[1] + " " + matrix[2] + " " + matrix[3]);
		Log.d("2:", " " + matrix[4] + " " + matrix[5] + " " + matrix[6] + " " + matrix[7]);
		Log.d("3: ", " " + matrix[8] + " " + matrix[9] + " " + matrix[10] + " " + matrix[11]);
		Log.d("4: ", " " + matrix[12] + " " + matrix[13] + " " + matrix[14] + " " + matrix[15]);
	}
	
	public static int createProgram(int vsid, int fsid, String[] attribs) {
		int vs = ShaderUtils.compileShader(GL_VERTEX_SHADER, ShaderUtils.getShaderSourceCode(context, vsid));
		int fs = ShaderUtils.compileShader(GL_FRAGMENT_SHADER, ShaderUtils.getShaderSourceCode(context, fsid));
		return ShaderUtils.createAndLinkProgram(vs, fs, attribs);
	}
	
	public void useProgram(int programHandle) {
		glUseProgram(programHandle);
	}
	
	public void useBasicProgram() {
		glUseProgram(basicProgramHandle);
	}

}
