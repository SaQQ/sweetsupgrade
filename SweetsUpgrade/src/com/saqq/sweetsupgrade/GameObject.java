package com.saqq.sweetsupgrade;

public interface GameObject {

	public void update(int t);
	
	public static final int POSITION_HANDLE = 0;
	public static final int COLOR_HANDLE = 1;
	public static final int NORMAL_HANDLE = 2;
	public static final int TEXTURE_HANDLE = 3;
	
	public void draw();
	
	
}
