package com.saqq.sweetsupgrade.objects;

import java.util.Observable;
import java.util.Observer;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.saqq.sweetsupgrade.GameObject;
import com.saqq.sweetsupgrade.objects.blocks.Block;
import com.saqq.sweetsupgrade.renderer.GLRect;
import com.saqq.sweetsupgrade.renderer.GLRenderer;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class ProgressDisplay implements GameObject, Observer {

	private static final float padding = 0.03f;
	private static float blockWidth;
	private static final int blockAmount = 15;
	private static float midOffset;

	private GLRect background;

	private float offset;
	private float sideSpeed;
	private static float sideAccel;

	private boolean hiding;
	private boolean showing;
	private boolean visible;

	private float width;
	private float height;

	private float xPos;
	private float yPos;

	private GLRect[] blocks;

	private static float speed;
	private int currMaxLevel;
	private float moveOffset;

	public static int mProgramHandle;
	private int mMinDist;
	private int mMaxDist;
	private int mCenter;
	private float[] center;
	private float[] minDist;
	private float[] maxDist;

	private int showTime;
	private boolean updated;

	public ProgressDisplay(final float offset) {

		// Quite funny but after 1 day i've got no idea
		// what in the name of god happens in this constructor
		// TO_DO: figure it out ;p

		width = Block.size * 1.5f;
		height = Block.size * 7.0f;

		blockWidth = width - 2 * padding;
		midOffset = (height - blockWidth) / 2;

		this.xPos = Board.xTiles * Block.size - width;
		this.yPos = offset + (Board.yTiles * Block.size - height) / 2;

		background = new GLRect(width, height);
		background.setTexture(TextureUtils.getTexture("progressbkg"));
		background.setAlpha(0.8f);

		blocks = new GLRect[blockAmount];
		for (int i = 0; i < blocks.length; i++) {
			blocks[i] = new GLRect(blockWidth, blockWidth);
			blocks[i].setTexture(TextureUtils.getTexture("block" + i));
			blocks[i].setPosition(padding, i * blockWidth);
		}

		currMaxLevel = 0;
		speed = blockWidth / 100;
		moveOffset = 0.0f;

		center = new float[] { 0.0f, height / 2, 0.0f, 1.0f };
		maxDist = new float[] { 0.0f, height, 0.0f, 1.0f };
		minDist = new float[] { 0.0f, height - blockWidth, 0.0f, 1.0f };

		mMinDist = GLES20.glGetUniformLocation(mProgramHandle, "u_mindist");
		mMaxDist = GLES20.glGetUniformLocation(mProgramHandle, "u_maxdist");
		mCenter = GLES20.glGetUniformLocation(mProgramHandle, "u_center");

		hiding = false;
		showing = false;
		this.offset = width;

		sideAccel = blockWidth / 600;
		sideSpeed = 0;
		visible = false;

		updated = false;
	}

	@Override
	public void update(int t) {
		if (moveOffset > 0) {
			moveOffset -= speed;
			if (moveOffset < 0) {
				moveOffset = 0.0f;
			}
		}

		if (showing) {
			sideSpeed += sideAccel * t;
			offset -= sideSpeed;
			if (offset < 0) {
				offset = 0;
				showing = false;
				visible = true;
				sideSpeed = 0;
			}
		} else if (hiding) {
			sideSpeed += sideAccel * t;
			offset += sideSpeed;
			if (offset > width) {
				offset = width;
				hiding = false;
				visible = false;
				sideSpeed = 0;
			}
		}

		if (updated) {
			showTime -= t;
			if (showTime < 0) {
				hide();
				updated = false;
			}
		}

	}

	float[] tmp = new float[4];

	@Override
	public void draw() {
		GLRenderer r = GLRenderer.getInstance();

		r.pushMatrix();
		
		r.translate(xPos + offset, yPos, 0.0f);

		background.draw();

		r.useProgram(mProgramHandle);

		Matrix.multiplyMV(tmp, 0, r.getMVPMatrix(), 0, center, 0);
		GLES20.glUniform4fv(mCenter, 1, tmp, 0);
		Matrix.multiplyMV(tmp, 0, r.getMVPMatrix(), 0, minDist, 0);
		GLES20.glUniform4fv(mMinDist, 1, tmp, 0);
		Matrix.multiplyMV(tmp, 0, r.getMVPMatrix(), 0, maxDist, 0);
		GLES20.glUniform4fv(mMaxDist, 1, tmp, 0);

		r.translate(0.0f, midOffset - currMaxLevel * blockWidth + moveOffset,
				0.0f);

		for (int i = 0; i < blocks.length; i++) {
			blocks[i].draw();
		}

		r.useBasicProgram();

		r.popMatrix();
	}

	public void show() {
		showing = true;
	}

	public void hide() {
		hiding = true;
	}

	public void toggle() {
		if (visible)
			hide();
		else
			show();
	}

	@Override
	public void update(Observable observable, Object data) {
		int maxLevel = ((Board) observable).getMaxLeveL();
		if (maxLevel > currMaxLevel)
			nextLevel();
	}

	public void nextLevel() {
		currMaxLevel++;
		moveOffset += blockWidth;
		if (!visible || updated) {
			showTime += 4000;
			updated = true;
			show();
		}
	}
	
	public void setMaxLevel(int level) {
		this.currMaxLevel = level;
	}

}
