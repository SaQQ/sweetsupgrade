package com.saqq.sweetsupgrade.objects.effects;

import com.saqq.sweetsupgrade.GameObject;
import com.saqq.sweetsupgrade.objects.Board;

public abstract class Effect implements GameObject {
	
	protected Board board;
	
	public boolean isDead;
	
	public Effect(Board board) {
		
		this.board = board;
		
		isDead = false;
		
	}
	
	public abstract void update(int t);
	
	public abstract void draw();
	
	public abstract void onAnimationEnd();
	
	public abstract void forceAnimationEnd();
	
}
