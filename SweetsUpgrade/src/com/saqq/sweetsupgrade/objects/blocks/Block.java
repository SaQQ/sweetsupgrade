package com.saqq.sweetsupgrade.objects.blocks;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.objects.blocks.animations.AcceleratingAnimation;
import com.saqq.sweetsupgrade.objects.blocks.animations.Animation;
import com.saqq.sweetsupgrade.objects.blocks.animations.DropAnimation;
import com.saqq.sweetsupgrade.renderer.GLRect;

public abstract class Block extends GLRect {
	
	//REFACTOR THIS SHIT!!!!!!!!
	
	public static float size;
	
	public static final float speed = 0.1f;
	
	protected final Board board;
	
	public int xPos;
	public int yPos;
	public float xOffset;
	public float yOffset;
	public int xTarget;
	public int yTarget;
	protected boolean moving; //Question: couldnt it serve as "moveFinished" boolean?
	protected boolean dropping;
	
	private Animation animation;
	public boolean moveFinished;
	
	public int dropDist;
	public int baseX;
	public int baseY;
	
	public Block(final Board board, int x, int y) {
		super(size, size);
		this.board = board;
		this.xPos = x;
		this.yPos = y;
		this.xOffset = 0.0f;
		this.yOffset = 0.0f;
		this.xTarget = x;
		this.yTarget = y;
		this.moving = false;
		this.dropping = false;
		updatePosition();
	}
	
	public void update(int t) {
		if(moving) {
			animation.tick(t);
			updatePosition();
		}
	}
	
	public void updatePosition() {
		setPosition(xPos*size + xOffset, yPos*size + yOffset);
	}
	
	protected void setBoardPosition(int x, int y) {
		
		if(x == xPos && y == yPos) return;
		if(x < 0 || y < 0 || x > Board.xTiles - 1 || y > Board.yTiles - 1)
			throw new RuntimeException("setBoardPosition() error: out of board bounds!");
		
		board.blocks[x][y] = this;
		board.blocks[xPos][yPos] = null;
		xPos = x;
		yPos = y;
	}
	
	public void endMove() {
		setBoardPosition(xTarget, yTarget);

		xOffset = 0.0f;
		yOffset = 0.0f;
		
		onMoveEnd();
		
		moving = false;
		dropping = false;

	}
	
	public void drop(final int depth) {
		if(moving) return;
		moving = true;
		dropping = true;
		
		this.xTarget = xPos;
		this.yTarget = yPos - depth;

		//int xDist = xTarget - xPos;
		int yDist = yTarget - yPos;
		
		moveFinished = false;
		this.animation = new DropAnimation(this, yDist);
		
		if(xTarget < 0 || xTarget > Board.xTiles - 1 || yTarget < 0 || yTarget > Board.yTiles - 1)
			endMove();
	}
	
	public void move(final int xDir, final int yDir) {
		if(moving) return;
		moving = true;
		
		this.xTarget = xPos + xDir;
		this.yTarget = yPos + yDir;

		int xDist = xTarget - xPos;
		int yDist = yTarget - yPos;
		
		moveFinished = false;
		this.animation = new AcceleratingAnimation(this, xDist, yDist);
		
		if(xTarget < 0 || xTarget > Board.xTiles - 1 || yTarget < 0 || yTarget > Board.yTiles - 1)
			endMove();
	}
	
	protected void onMoveEnd() {
		moveFinished = true;
	}
	
	public void forceMoveEnd() {
		animation.forceAnimationEnd();
	}
	
	@Override
	public String toString() {
		String result = "Block: xPos: " + xPos + " yPos: " + yPos + "\n";
		return result;
	}

}
