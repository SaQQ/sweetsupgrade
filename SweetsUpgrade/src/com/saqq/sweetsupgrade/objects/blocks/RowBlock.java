package com.saqq.sweetsupgrade.objects.blocks;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.objects.effects.RowEffect;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class RowBlock extends Superblock {

	public RowBlock(Board board, int x, int y) {
		super(board, x, y);
		setTexture(TextureUtils.getTexture("superblock1"));
	}
	
	protected void onMoveEnd() {
		if(dropping)
			board.addEffect(new RowEffect(board, this)); 
		else
			this.moveFinished = true;
	}
	
	
}
