package com.saqq.sweetsupgrade.objects.blocks;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.objects.effects.BombEffect;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class BombBlock extends Superblock {

	public BombBlock(Board board, int x, int y) {
		super(board, x, y);
		setTexture(TextureUtils.getTexture("superblock0"));
	}
	
	protected void onMoveEnd() {
		if(dropping)
			board.addEffect(new BombEffect(board, this)); 
		else
			this.moveFinished = true;
	}
	
	
}
