package com.saqq.sweetsupgrade.objects.blocks.animations;

import com.saqq.sweetsupgrade.objects.blocks.Block;

public class AcceleratingAnimation extends Animation {

	private final static float acceleration = Block.size/20000;
	
	private float xSpeed;
	private float ySpeed;
	
	private float xAcc;
	private float yAcc;
	
	private final float xBound;
	private final float yBound;

	public AcceleratingAnimation(Block block, int xDist, int yDist) {
		super(block);
		
		ySpeed = 0.0f;
		xSpeed = 0.0f;
		
		xAcc = acceleration * xDist;
		yAcc = acceleration * yDist;

		xBound = Math.abs(xDist * Block.size);
		yBound = Math.abs(yDist * Block.size);
	}

	@Override
	public void tick(int t) {
		xSpeed += t * xAcc;
		ySpeed += t * yAcc;
		
		block.xOffset += xSpeed * t;
		block.yOffset += ySpeed * t;
		if(Math.abs(block.xOffset) >= xBound && Math.abs(block.yOffset) >= yBound)
			endAnimation();
	}

}
