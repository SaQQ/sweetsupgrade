package com.saqq.sweetsupgrade.objects.blocks.animations;

import android.util.Log;

import com.saqq.sweetsupgrade.objects.blocks.Block;

public class SmoothAnimation extends Animation {

	private final static float vmax = Block.size/200;
	private final static float vmin = Block.size/2000;
	
	private float xSpeed;
	private float ySpeed;
	
	private final float xMaxSpeed;
	private final float yMaxSpeed;
	private final float xMinSpeed;
	private final float yMinSpeed;

	private final float xBound;
	private final float yBound;

	public SmoothAnimation(Block block, int xDist, int yDist) {
		super(block);
		
		ySpeed = 0.0f;
		xSpeed = 0.0f;
		
		xMaxSpeed = vmax*xDist;
		yMaxSpeed = vmax*yDist;
		xMinSpeed = vmin*xDist;
		yMinSpeed = vmin*yDist;
		
		xBound = xDist * Block.size;
		yBound = yDist * Block.size;
		
		block.xOffset = 0.001f;
		block.yOffset = 0.001f;
	}

	@Override
	public void tick(int t) {
	
		//**********************WHY_THIS_NOT_WORK??????????**********************//
		xSpeed = (float) (Math.sin(/*Math.PI **/ (block.xOffset/xBound))*xMaxSpeed);
		ySpeed = (float) (Math.sin(/*Math.P * */(block.yOffset/yBound))*yMaxSpeed);
		
		if(xSpeed < xMinSpeed)
			xSpeed = xMinSpeed;
		if(ySpeed < yMinSpeed)
			ySpeed = yMinSpeed;
		
		Log.e("ANIMATION", "x: " + xSpeed + " y: " + ySpeed);
		
		block.xOffset += xSpeed * t;
		block.yOffset += ySpeed * t;
		if(Math.abs(block.xOffset) >= Math.abs(xBound) && Math.abs(block.yOffset) >= Math.abs(yBound))
			endAnimation();
	}

}
