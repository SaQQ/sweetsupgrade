package com.saqq.sweetsupgrade.objects.blocks.animations;

import com.saqq.sweetsupgrade.objects.blocks.Block;

public abstract class Animation {
	
	protected Block block;

	public Animation(Block block) {
		this.block = block;
	}
	
	public abstract void tick(int t);
	
	protected void endAnimation() {
		block.endMove();
	}
	
	public void forceAnimationEnd() {
		this.endAnimation();
	}
}
