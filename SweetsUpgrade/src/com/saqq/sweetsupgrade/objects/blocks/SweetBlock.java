package com.saqq.sweetsupgrade.objects.blocks;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class SweetBlock extends Block {
	
	private int level;

	public SweetBlock(final Board board, int x, int y) {
		super(board, x, y);
		setLevel(0);
	}
	
	public SweetBlock(final Board board, int x, int y, int level) {
		super(board, x, y);
		setLevel(level);
	}
	
	public void setLevel(int level) {
		this.level = level;
		setTexture(TextureUtils.getTexture("block" + level));
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public int incrementLevel() {
		setLevel(++level);
		return level;
	}
	
	public String toString() {
		String result = "Block: xPos: " + xPos + " yPos: " + yPos + " level: " + level + "\n";
		return result;
	}
	
}
