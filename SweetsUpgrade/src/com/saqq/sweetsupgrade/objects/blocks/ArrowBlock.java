package com.saqq.sweetsupgrade.objects.blocks;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.objects.effects.ArrowEffect;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class ArrowBlock extends Superblock {

	public ArrowBlock(Board board, int x, int y) {
		super(board, x, y);
		setTexture(TextureUtils.getTexture("superblock2"));
	}
	
	protected void onMoveEnd() {
		if(dropping)
			board.addEffect(new ArrowEffect(board, this)); 
		else
			this.moveFinished = true;
	}
	
	
}
