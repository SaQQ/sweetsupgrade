package com.saqq.sweetsupgrade.objects.blocks;

import com.saqq.sweetsupgrade.objects.Board;
import com.saqq.sweetsupgrade.renderer.TextureUtils;

public class BackgroundBlock extends Block {
	
	public BackgroundBlock(final Board board, int x, int y, boolean starting) {
		super(board, x, y);	
		if(starting)
			setTexture(TextureUtils.getTexture("block18"));
		else 
			setTexture(TextureUtils.getTexture("block19"));
	}

	@Override
	public void update(int t) {
		super.update(t);
	}

}
